package com.test.demoTest.DaoImpliment;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.ws.rs.core.Response;

import org.apache.el.lang.LambdaExpressionNestedState;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.test.demoTest.Dao.LoanDao;
import com.test.demoTest.Model.Loan;

public class LoanDaoImpliment extends BaseDao implements LoanDao {

	@Override
	public Loan getLoan(String nic) {

		Loan ln = null;
		String sql = "call GetLoan(?)";

		try {
			Connection con = getConn();
			CallableStatement cs = con.prepareCall(sql);
			cs.setString(1, nic);
			ResultSet rs = cs.executeQuery();

			while (rs.next()) {
				ln = new Loan();
				ln.setAmount(rs.getString(1));
				ln.setName(rs.getString(2));

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ln;
	}

	@Override
	public Loan getActiveLoan(String nic) {
		Loan ln = null;
		Connection connection = getConn();
		String sql = "SELECT AMOUNT,NAME FROM Loan WHERE NIC = ? AND Lstatus = '1'";

		try {
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, nic);
			ResultSet rSet = ps.executeQuery();
			while (rSet.next()) {
				ln = new Loan();
				ln.setAmount(rSet.getString(1));
				ln.setName(rSet.getString(2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ln;
	}

	@Override
	public Response create(Loan loan) {
		String sql = "INSERT INTO Loan (CUSTOMER_ID,NAME,nic,amount,Lstatus,type) VALUES (?,?,?,?,?,?)";

		Loan ln = null;
		try {

			Connection con = getConn();
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, loan.getCustomerId());
			ps.setString(2, loan.getName());

			ps.setString(3, loan.getNic());
			ps.setString(4, loan.getAmount());
			ps.setInt(5, loan.getlStatus());
			ps.setInt(6, loan.getType());

			ps.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
//			Logger.info("error occured when create loan");
//			Logger.error(e.getMessage());
//			e.getMessage();

		}
		return Response.ok(ln).build();
	}

	@Override
	public Response settleLoan(Loan loan) {

		String sql = "";
		try {
			Connection connection = getConn();

			PreparedStatement ps = connection.prepareStatement(sql);

		} catch (Exception e) {

		}
		return null;
	}

}
