package com.test.demoTest.DaoImpliment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.lang.model.element.PackageElement;

import org.springframework.beans.factory.annotation.Autowired;

import com.test.demoTest.Dao.CustomerDao;
import com.test.demoTest.Model.Customer;

public class CustomerDaoImpliment extends BaseDao implements CustomerDao {

	@Override
	public List<Customer> getAll() {
		List<Customer> list = new ArrayList<Customer>();
		String sql = "SELECT name,nic FROM `Customer` ";
		System.out.println("11111");
		try {

			Connection conne = getConn();
			PreparedStatement ps = conne.prepareStatement(sql);
			System.out.println(ps);
			ResultSet rSet = ps.executeQuery();

			while (rSet.next()) {
				Customer customer = new Customer();
				customer.setName(rSet.getString(1));
				customer.setNic(rSet.getString(2));
				list.add(customer);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public Customer getCusByNic(String nic) {
		Customer cus = new Customer();

		String sql = "SELECT name,nic FROM `Customer` WHERE nic=?";
		try {
			Connection connection = getConn();
			PreparedStatement ps = connection.prepareStatement(sql);
			ps.setString(1, nic);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				cus = new Customer();
				cus.setName(rs.getString(1));
				cus.setNic(rs.getString(2));
				cus.setNic(nic);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return cus;

	}
}
