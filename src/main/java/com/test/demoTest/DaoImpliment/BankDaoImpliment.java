package com.test.demoTest.DaoImpliment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.springframework.beans.factory.annotation.Autowired;

import com.test.demoTest.Dao.BankDao;
import com.test.demoTest.Model.Bank;

public class BankDaoImpliment extends BaseDao implements BankDao {

//	@Autowired
//	public Bank bank;

	@Override
	public Bank getBank() {

		Bank bank = new Bank();

		String sql = "SELECT BankName FROM Bank WHERE 1 ";

		try {
			Connection connection = getConn();
			PreparedStatement ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Bank bnk = new Bank();
//				bnk.setBankName(rs.getString(1));
				bank.setBankName(rs.getString(1));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return bank;
	}

}
