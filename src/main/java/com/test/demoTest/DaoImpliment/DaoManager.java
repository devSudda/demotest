package com.test.demoTest.DaoImpliment;

import com.test.demoTest.Dao.BankDao;
import com.test.demoTest.Dao.CustomerDao;
import com.test.demoTest.Dao.LoanDao;
import com.test.demoTest.Dao.UserDao;

public class DaoManager {

	public static CustomerDao cusdao() {
		return new CustomerDaoImpliment();

	}

	public static LoanDao loandao() {
		return new LoanDaoImpliment();
	}

	public static BankDao bnkDao() {
		return new BankDaoImpliment();
	}

	public static UserDao userDao() {
		return new UserDaoImpliment();
	}
}
