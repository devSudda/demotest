package com.test.demoTest.DaoImpliment;

import java.sql.Connection;

import org.springframework.context.annotation.Configuration;

import com.test.demoTest.DbConnection.DbConfig;

@Configuration
public class BaseDao {

	public Connection getConn() {
		return DbConfig.MANAGER.getConnection();
	}
}
