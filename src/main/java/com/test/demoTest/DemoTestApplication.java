package com.test.demoTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.delivery.request" })
@EntityScan("com.delivery.domain")
@ComponentScan(basePackages = { "com.test.demoTest" })

public class DemoTestApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(DemoTestApplication.class, args);
		System.out.println("Demotest Application ON");
	}

	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(DemoTestApplication.class);
	}

}
