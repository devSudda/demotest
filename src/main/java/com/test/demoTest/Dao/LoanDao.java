package com.test.demoTest.Dao;

import javax.ws.rs.core.Response;

import com.test.demoTest.Model.Loan;

public interface LoanDao {

	public Loan getLoan(String nic);

	public Loan getActiveLoan(String nic);

//	public boolean addLoan(Loan loan);
	public Response create(Loan loan);

	public Response settleLoan(Loan loan);

}
