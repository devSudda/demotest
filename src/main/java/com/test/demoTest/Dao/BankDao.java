package com.test.demoTest.Dao;

import com.test.demoTest.Model.Bank;

public interface BankDao {

	public Bank getBank();

}
