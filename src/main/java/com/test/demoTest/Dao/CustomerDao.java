package com.test.demoTest.Dao;

import java.util.List;

import com.test.demoTest.Model.Customer;

public interface CustomerDao {

	public List<Customer> getAll();

	public Customer getCusByNic(String nic);
}
