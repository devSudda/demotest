package com.test.demoTest;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.OkHttp3ClientHttpRequestFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.demoTest.Model.Loan;
import com.test.demoTest.Service.LoanService;

@RestController
@RequestMapping("/loan")
@CrossOrigin(origins = "*")
public class LoanController {

	@Autowired
	private LoanService ls;

	@Autowired
	private Loan ln;

	@GetMapping("/getLoan/{nic}")
	public Loan getLoan(@PathVariable("nic") String nic) {
		return ls.getLoan(nic);
	}

//	@GetMapping("/getactive/{nic}")
//	public Loan getActiveLoan(@PathVariable("nic") String nic) {
//		return ls.getActiveLoan(nic);
//	}

	@GetMapping("/getactive/{nic}")
	public Response getActiveLoan(@PathVariable("nic") String nic) {

		ln = ls.getActiveLoan(nic);

		return Response.ok(ln).build();

	}

	@PostMapping("add")
	public Response addLoan(@RequestBody Loan loan) {
		ls.create(loan);
		System.out.println(loan.getName());
		return Response.ok().build();
	}

}
