package com.test.demoTest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.demoTest.Model.Bank;
import com.test.demoTest.Service.BankService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("bank")

public class BankController {

	@Autowired
	public BankService bs;

	// test123

	@GetMapping("/all")
	public Bank getAll() {
		return bs.getbankLogging();
	}

}
