package com.test.demoTest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.demoTest.Model.Customer;
import com.test.demoTest.Service.CustomerService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/customer")
public class CustomerController {

	@Autowired
	public CustomerService cus;

	@GetMapping("/all")
	public List<Customer> getAll() {
		System.out.println("test");
		return cus.getAll();
	}

	// test2

	@GetMapping("/getcusByNic/{nic}")
	public Customer getCusByNic(@PathVariable("nic") String nic) {
		return cus.getCusByNic(nic);

	}

}
