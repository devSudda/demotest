package com.test.demoTest.Service;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.test.demoTest.Model.User;

@Component
public interface UserService {

	public boolean addUser(User user);
}
