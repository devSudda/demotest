package com.test.demoTest.Service;

import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.test.demoTest.Model.Loan;

@Component
public interface LoanService {

	public Loan getLoan(String nic);

	public Loan getActiveLoan(String nic);

//	public boolean addLoan(Loan loan);

	public Response create(Loan loan);

	public Response setteleLoan(Loan loan);
}
