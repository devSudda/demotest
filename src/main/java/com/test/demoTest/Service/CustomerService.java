package com.test.demoTest.Service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.test.demoTest.Model.Customer;

@Component
public interface CustomerService {

	public List<Customer> getAll();

	public Customer getCusByNic(String nic);
}
