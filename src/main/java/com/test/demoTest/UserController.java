package com.test.demoTest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.demoTest.Model.User;
import com.test.demoTest.Service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserService UserService;

	@PostMapping("/add")
	public boolean addUser(@RequestBody User user) {
		Boolean boolean1 = null;
		if (true == UserService.addUser(user)) {
			boolean1 = true;
		} else {
			boolean1 = false;
		}

		return boolean1;

	}

}
