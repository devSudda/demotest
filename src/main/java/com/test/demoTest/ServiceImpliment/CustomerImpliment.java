package com.test.demoTest.ServiceImpliment;

import java.util.List;

import org.springframework.stereotype.Service;

import com.test.demoTest.DaoImpliment.DaoManager;
import com.test.demoTest.Model.Customer;
import com.test.demoTest.Service.CustomerService;

@Service
public class CustomerImpliment implements CustomerService {

	@Override
	public List<Customer> getAll() {

		System.out.println("ins cus implimennt");
		return DaoManager.cusdao().getAll();
	}

	@Override
	public Customer getCusByNic(String nic) {

		return DaoManager.cusdao().getCusByNic(nic);
	}

}
