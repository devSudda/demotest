package com.test.demoTest.ServiceImpliment;

import javax.ws.rs.core.Response;

import org.apache.logging.log4j.message.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.demoTest.DaoImpliment.DaoManager;
import com.test.demoTest.Model.Loan;
import com.test.demoTest.Service.LoanService;

@Service
public class LoanImpliment implements LoanService {

	@Autowired
	public Loan ln;

	@Override
	public Loan getLoan(String nic) {

		return DaoManager.loandao().getLoan(nic);
	}

	@Override
	public Loan getActiveLoan(String nic) {

		ln = DaoManager.loandao().getActiveLoan(nic);

		if (ln == null) {

			System.out.println("no loans for nic");
		}
		return ln;
	}

//	@Override
//	public boolean addLoan(Loan loan) {
//
//		DaoManager.loandao().addLoan(loan);
//		return true;
//	}

	@Override
	public Response create(Loan loan) {

		return DaoManager.loandao().create(loan);
	}

	@Override
	public Response setteleLoan(Loan loan) {

		return null;
//		return DaoManager.loandao().settle(loan);
	}

}
