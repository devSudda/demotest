package com.test.demoTest.ServiceImpliment;

import org.springframework.stereotype.Service;

import com.test.demoTest.DaoImpliment.DaoManager;
import com.test.demoTest.Model.Bank;
import com.test.demoTest.Service.BankService;

@Service
public class BankServiceImpliment implements BankService {

	public Bank getbankLogging() {

		return DaoManager.bnkDao().getBank();
	}

}
