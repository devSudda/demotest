package com.test.demoTest.Model;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class Loan {

	private String amount;
	private int type;
	private String name;
	private int customerId;
	private String nic;
	private Date date;
	private int lStatus;
	private int id;

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getlStatus() {
		return lStatus;
	}

	public void setlStatus(int lStatus) {
		this.lStatus = lStatus;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

}
