package com.test.demoTest.DbConnection;

import java.sql.Connection;
import java.sql.DriverManager;

public enum DbConfig {

	MANAGER;

	static final String JDBC_DRIVER_STRING = "com.mysql.jdbc.Driver";
	static final String DB_URL_STRING = "jdbc:mysql://192.168.100.150:3306/test";

	public static final String USER = "sasini";
	public static final String PASS = "sasini";

	public Connection getConnection() {
		Connection conn = null;
		try {

			Class.forName(JDBC_DRIVER_STRING).newInstance();
			conn = DriverManager.getConnection(DB_URL_STRING, USER, PASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;

	}
}
